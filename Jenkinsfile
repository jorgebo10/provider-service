def nextSnapshotVersion
def releaseVersion
def releaseTag

pipeline {
    agent {
        docker {
            image 'jorgebo10/maven-git:1.9-alpine' 
            args '-v /var/jenkins_home/.m2/settings.xml:/root/.m2/settings.xml --network nexus-net -e "NEXUS_USERNAME=admin" -e "NEXUS_PASSWORD=admin123" -e "NEXUS_URL=http://nexus:8081"'
        }
    }
    
    stages {
        stage('Build') {
            steps {
                sh 'mvn -B -DskipTests clean package'
            }
        }
        stage('Test') {
            steps {
                sh 'env'
                sh 'mvn test'
            }
        }
         stage('Releasing for dev') {
            when {
                branch 'development'
            }
            steps {
                echo 'pubish to dev nexus snapshot version'
                echo 'pubish to dev docker snapshot image'
            }
        }
        stage('Releasing for stage') {
            when {
                branch 'master'
            }
            steps {
                script {    
                    def pom = readMavenPom file: 'pom.xml'
                    def pomName = pom.name
                    def ver = pom.version.replace("-SNAPSHOT", "").split("\\.")
                    ver[-2] = ver[-2].toInteger() + 1
                    
                    nextSnapshotVersion = ver.join('.') + "-SNAPSHOT"
                    def suggestedReleaseVersion = pom.version.replace("-SNAPSHOT", "")

                    def semVersionInput = input(
                        id: 'semVersionInput', 
                        message: 'Enter semantic version',
                        parameters: [
                            string(defaultValue: suggestedReleaseVersion,
                            description: 'Semantic version e.g.: mayer.minor.patch',
                            name: 'semVersion')])
    
                    releaseVersion = semVersionInput?:suggestedReleaseVersion
                    releaseTag=pomName + '-' + releaseVersion
    
                    echo "Releasing with semVersion: ${releaseVersion} tag with ${releaseTag}"
                }

                sh "mvn -B -Dtag=${releaseTag} \
                           -DreleaseVersion=${releaseVersion} \
                           -DdevelopmentVersion=${nextSnapshotVersion} \
                           -Dresume=false \
                           release:prepare \
                           release:perform"
                echo "publishing to nexus"
                echo "cooking docker image with version"
                echo "pushing docker image with relVersion"
            }
        }
        stage('AT') {
            when {
                branch 'master' 
            }
            steps {
                echo "at"
            }
        }
        stage('Deploying to dev') {
            when {
                branch 'development' 
            }
            steps {
                echo "executing deploy script to deploy docker image with: docker:snapshot version"
            }
        }
        stage('Deploying to stage') {
            when {
                branch 'master' 
            }
            steps {
                echo "executing deploy script to deploy docker image with: docker:${releaseVersion}"
            }
        }
    }
}