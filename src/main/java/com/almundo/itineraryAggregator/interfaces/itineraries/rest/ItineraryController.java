package com.almundo.itineraryAggregator.interfaces.itineraries.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import com.almundo.itineraryAggregator.application.aggregateByPrice.ItineraryPriceAggregatorService;
import com.almundo.itineraryAggregator.domain.itineray.Itinerary;
import com.almundo.itineraryAggregator.domain.itineray.ItinerarySearchRequest;
import com.almundo.itineraryAggregator.domain.itineray.Product;
import com.almundo.itineraryAggregator.domain.provider.Provider;
import io.reactivex.Observable;

@RestController
@RequestMapping(value = "/itineraries")
public class ItineraryController {
    private static Logger logger = LoggerFactory.getLogger(ItineraryController.class);
    private final List<Provider> providers;
    private final ItineraryPriceAggregatorService itineraryPriceAggregatorService;

    @Autowired
    public ItineraryController(final List<Provider> providers, final ItineraryPriceAggregatorService itineraryPriceAggregatorService) {
        this.providers = providers;
        this.itineraryPriceAggregatorService = itineraryPriceAggregatorService;
    }

    @PostMapping(value = "/cheaperProviderSearchResult")
    public DeferredResult<Itinerary> cheaperItinerary(/*@RequestBody final ItinerarySearchRequest itinerarySearchRequest*/) {
        logger.info("Start Thread: {}" + Thread.currentThread().getName());

        ItinerarySearchRequest itinerarySearchRequest= ItinerarySearchRequest.create("ROM", "ARG", "230918", 4, 4, 4, 4, Product.FLIGHT);

        final Observable<Itinerary> itineraryObservable = itineraryPriceAggregatorService.cheaperItineraryByProvider(itinerarySearchRequest, providers);

        final DeferredResult<Itinerary> deferredResult = new DeferredResult<>();
        itineraryObservable.subscribe(deferredResult::setResult, deferredResult::setErrorResult);
        logger.info("Exit Thread: {}" + Thread.currentThread().getName());

        return deferredResult;
    }

}