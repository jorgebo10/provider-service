package com.almundo.itineraryAggregator.domain.provider;

import java.util.List;

import com.almundo.itineraryAggregator.domain.itineray.Itinerary;
import com.almundo.itineraryAggregator.domain.itineray.ProviderItinerariesSearchRequest;
import io.reactivex.Observable;

public interface ProviderService {

    Observable<List<Itinerary>> findItinearies(final ProviderItinerariesSearchRequest providerItinerariesSearchRequest);
}
