package com.almundo.itineraryAggregator.domain.provider;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class  Provider {
    public static Provider create(String url, String name) {
        return new AutoValue_Provider(url, name);
    }
    public abstract String url();
    public abstract String name();
}
