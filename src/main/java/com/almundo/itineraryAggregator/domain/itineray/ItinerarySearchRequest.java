package com.almundo.itineraryAggregator.domain.itineray;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class ItinerarySearchRequest {
    public static ItinerarySearchRequest create(final String from, final String to, final String departure, Integer adults, Integer children, Integer infants, Integer amount, Product product) {
        return new AutoValue_ItinerarySearchRequest(from, to, departure, adults, children, infants, amount, product);
    }
    public abstract String from();
    public abstract String to();
    public abstract String departure();
    public abstract Integer adults();
    public abstract Integer children();
    public abstract Integer infants();
    public abstract Integer amount();
    public abstract Product product();
}
