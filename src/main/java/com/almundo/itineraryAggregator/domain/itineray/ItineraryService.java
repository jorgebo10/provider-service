package com.almundo.itineraryAggregator.domain.itineray;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.almundo.itineraryAggregator.domain.provider.ProviderService;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

@Service
public class ItineraryService {
    private final ProviderService providerService;
    private final ItineraryAggretateSelector itineraryAggretateSelector;

    @Autowired
    public ItineraryService(final ProviderService providerService,
                            final ItineraryAggretateSelector itineraryAggretateSelector) {
        this.providerService = providerService;
        this.itineraryAggretateSelector = itineraryAggretateSelector;
    }

    public Observable<Itinerary> cheaperItineraryByProvider(final List<ProviderItinerariesSearchRequest> providerItinerarySearchRequest) {
        final List<Observable<List<Itinerary>>> itinerariesByProvider = providerItinerarySearchRequest.stream()
                .map(providerService::findItinearies)
                .map(listObservable -> listObservable.subscribeOn(Schedulers.io()))
                .collect(Collectors.toList());

        return Observable.zip(itinerariesByProvider, itineraryAggretateSelector.cheaperItinerary())
                .cast(Itinerary.class);
    }

    public static void printData(Object object) {
        System.out.println("Thread: " + Thread.currentThread());
    }
}
