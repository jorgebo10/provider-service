package com.almundo.itineraryAggregator.domain.itineray;

import java.math.BigDecimal;

public class Itinerary {

    private String provider;
    private BigDecimal price;

    public Itinerary(String provider, BigDecimal price) {
        this.provider = provider;
        this.price = price;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getProvider() {
        return provider;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Itinerary{");
        sb.append("itineraryAggregator='").append(provider).append('\'');
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }
}
