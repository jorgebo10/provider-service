package com.almundo.itineraryAggregator.domain.itineray;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import io.reactivex.functions.Function;

@Component
public final class ItineraryAggretateSelector {

    public Function<Object[], Object> cheaperItinerary() {
        return objects -> {
            List<Itinerary> allEvents = new ArrayList<>();
            for (Object o1 : objects) {
                List<Itinerary> le = (List<Itinerary>) o1;
                allEvents.addAll(le);
            }
            return allEvents.stream().min((o1, o2) -> o1.getPrice().compareTo(o2.getPrice())).get();
        };
    }

    public Function<Object[], Object> mostExpensiveItinerary() {
        return objects -> {
            List<Itinerary> allEvents = new ArrayList<>();
            for (Object o1 : objects) {
                List<Itinerary> le = (List<Itinerary>) o1;
                allEvents.addAll(le);
            }
            return allEvents.stream().max((o1, o2) -> o1.getPrice().compareTo(o2.getPrice())).get();
        };
    }
}
