package com.almundo.itineraryAggregator.domain.itineray;

import com.almundo.itineraryAggregator.domain.provider.Provider;
import com.google.auto.value.AutoValue;

@AutoValue
public abstract class ProviderItinerariesSearchRequest {
    public static ProviderItinerariesSearchRequest create(final ItinerarySearchRequest itinerarySearchRequest, final Provider provider) {
        return new AutoValue_ProviderItinerariesSearchRequest(itinerarySearchRequest, provider);
    }
    public abstract ItinerarySearchRequest searchRequest();
    public abstract Provider provider();
}
