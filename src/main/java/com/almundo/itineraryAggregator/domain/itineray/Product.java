package com.almundo.itineraryAggregator.domain.itineray;

public enum Product {
    FLIGHT,
    TRIP;
}
