package com.almundo.itineraryAggregator.application.aggregateByPrice;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.almundo.itineraryAggregator.domain.itineray.Itinerary;
import com.almundo.itineraryAggregator.domain.itineray.ItinerarySearchRequest;
import com.almundo.itineraryAggregator.domain.itineray.ItineraryService;
import com.almundo.itineraryAggregator.domain.itineray.ProviderItinerariesSearchRequest;
import com.almundo.itineraryAggregator.domain.provider.Provider;
import io.reactivex.Observable;

@Service
public class ItineraryPriceAggregatorService {
    private final ItineraryService itineraryService;

    @Autowired
    public ItineraryPriceAggregatorService(final ItineraryService itineraryService) {
        this.itineraryService = itineraryService;
    }

    public Observable<Itinerary> cheaperItineraryByProvider(final ItinerarySearchRequest itinerarySearchRequest, final List<Provider> providers) {
        final List<ProviderItinerariesSearchRequest> providerItinerarySearchRequests = providers.stream()
                .map(provider -> ProviderItinerariesSearchRequest.create(itinerarySearchRequest, provider))
                .collect(Collectors.toList());

        final Observable<Itinerary> itineraryObservable = Optional.ofNullable(providerItinerarySearchRequests)
                .map(itineraryService::cheaperItineraryByProvider)
                .get();

        return itineraryObservable;

        //TODO: Subscribe to other events such as notifications, storables
    }
}
