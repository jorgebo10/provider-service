package com.almundo.itineraryAggregator.infrastructure.config;

import static java.math.BigDecimal.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.almundo.itineraryAggregator.application.aggregateByPrice.ItineraryPriceAggregatorService;
import com.almundo.itineraryAggregator.domain.itineray.Itinerary;
import com.almundo.itineraryAggregator.domain.itineray.ItineraryAggretateSelector;
import com.almundo.itineraryAggregator.domain.itineray.ItineraryService;
import com.almundo.itineraryAggregator.domain.provider.Provider;
import com.almundo.itineraryAggregator.domain.provider.ProviderService;
import com.almundo.itineraryAggregator.infrastructure.providers.MockProviderService;

@Configuration
@EnableWebMvc
@Profile(value = "mock")
public class MockConfig {
    @Bean
    public ItineraryPriceAggregatorService itineraryPriceAggregatorService(final ItineraryService itineraryService) {
        return new ItineraryPriceAggregatorService(itineraryService);
    }

    @Bean
    public ItineraryService itineraryService(final ProviderService providerService, final ItineraryAggretateSelector itineraryAggretateSelector) {
        return new ItineraryService(providerService, itineraryAggretateSelector);
    }

    @Bean
    public ItineraryAggretateSelector itineraryAggretateSelector() {
        return new ItineraryAggretateSelector();
    }

    @Bean
    public List<Provider> providers() {
        return Arrays.asList(
                Provider.create("amadeus", "amadeus"),
                Provider.create("sabre", "sabre"),
                Provider.create("wor", "wor"));
    }

    @Bean
    public Map<Provider, List<Itinerary>> providerItineraries(final List<Provider> providers) {
        final Map<Provider, List<Itinerary>> providerItineraries = new HashMap<>();
        providerItineraries.put(providers.get(0), Arrays.asList(new Itinerary("A1", valueOf(1.3)), new Itinerary("A2", valueOf(3.8))));
        providerItineraries.put(providers.get(1), Arrays.asList(new Itinerary("S1", valueOf(1.4)), new Itinerary("S2", valueOf(3.3)), new Itinerary("S3", valueOf(1.0))));
        providerItineraries.put(providers.get(2), Arrays.asList(new Itinerary("W1", valueOf(1.6)), new Itinerary("W2", valueOf(5.2)), new Itinerary("W3", valueOf(3.1))));
        return providerItineraries;
    }

    @Bean
    public ProviderService providerService(final Map<Provider, List<Itinerary>>  providerItineraries) {
            return new MockProviderService(providerItineraries);
    }
}
