package com.almundo.itineraryAggregator.infrastructure.providers;

import static java.util.Collections.*;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.almundo.itineraryAggregator.domain.itineray.Itinerary;
import com.almundo.itineraryAggregator.domain.itineray.ProviderItinerariesSearchRequest;
import com.almundo.itineraryAggregator.domain.provider.Provider;
import com.almundo.itineraryAggregator.domain.provider.ProviderService;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

@Service
public class MockProviderService implements ProviderService {
    private static final Logger logger = LoggerFactory.getLogger(MockProviderService.class);
    private final Map<Provider, List<Itinerary>> providerItineraries;

    @Autowired
    public MockProviderService(final Map<Provider, List<Itinerary>> providerItineraries) {
        this.providerItineraries = providerItineraries;
    }

    @Override
    public Observable<List<Itinerary>> findItinearies(final ProviderItinerariesSearchRequest providerItinerariesSearchRequest) {

        Observable<List<Itinerary>> listObservable = Observable.<List<Itinerary>>create(sub -> {
            try {
                sub.onNext(unmodifiableList(providerItineraries.get(providerItinerariesSearchRequest.provider())));
                sub.onComplete();
            } catch (RestClientException e) {
                sub.onError(new ProviderItineraryException("Unable to reach provider",
                        providerItinerariesSearchRequest.provider()));
            } catch (final Exception e) {
                sub.onError(e);
            }
        }).subscribeOn(Schedulers.io());

        listObservable.subscribe(itineraries -> logger.info("Thread: {}" + Thread.currentThread().getName()));
        return listObservable;
    }
}
