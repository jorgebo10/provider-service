package com.almundo.itineraryAggregator.infrastructure.providers;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.almundo.itineraryAggregator.domain.itineray.Itinerary;
import com.almundo.itineraryAggregator.domain.itineray.ProviderItinerariesSearchRequest;
import com.almundo.itineraryAggregator.domain.provider.ProviderService;
import io.reactivex.Observable;

@Service
public class RestProviderService implements ProviderService {
    private final RestTemplate restTemplate;

    @Autowired
    public RestProviderService(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public Observable<List<Itinerary>> findItinearies(final ProviderItinerariesSearchRequest providerItinerariesSearchRequest) {
        return Observable.create(sub -> {
            try {
                final ResponseEntity<Itinerary[]> itineraries = restTemplate.getForEntity(
                        providerItinerariesSearchRequest.provider().url(), Itinerary[].class);
                sub.onNext(Arrays.asList(itineraries.getBody()));
                sub.onComplete();
            } catch (RestClientException e) {
                sub.onError(new ProviderItineraryException("Unable to reach itineraryAggregator", providerItinerariesSearchRequest.provider()));
            } catch (Exception e) {
                sub.onError(e);
            }
        });
    }
}
