package com.almundo.itineraryAggregator.infrastructure.providers;

import com.almundo.itineraryAggregator.domain.provider.Provider;

public class ProviderItineraryException extends RuntimeException {
    private Provider provider;

    public ProviderItineraryException(String msg, Provider provider) {
        super(msg);
        this.provider = provider;
    }

    public Provider getProvider() {
        return provider;
    }
}
