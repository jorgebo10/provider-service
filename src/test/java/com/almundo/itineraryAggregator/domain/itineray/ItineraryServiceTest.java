package com.almundo.itineraryAggregator.domain.itineray;

import static java.math.BigDecimal.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import com.almundo.itineraryAggregator.domain.provider.Provider;
import com.almundo.itineraryAggregator.infrastructure.providers.MockProviderService;

public class ItineraryServiceTest {
    private ItineraryService itineraryService;
    private ItinerarySearchRequest itinerarySearchRequest;

    private List<Provider> providers = new ArrayList<>();
    private List<ProviderItinerariesSearchRequest> providerItinerarySearchRequest = new ArrayList<>();


    @Before
    public void setup() {
        itinerarySearchRequest= ItinerarySearchRequest.create("ROM", "ARG", "230918", 4, 4, 4, 4, Product.FLIGHT);
        providers.add(Provider.create("amadeus", "amadeus"));
        providers.add(Provider.create("sabre", "sabre"));
        providers.add(Provider.create("wor", "wor"));

        final Map<Provider, List<Itinerary>> providerItineraries = new HashMap<>();
        providerItineraries.put(providers.get(0), Arrays.asList(new Itinerary("A1", valueOf(1.3)), new Itinerary("A2", valueOf(3.8))));
        providerItineraries.put(providers.get(1), Arrays.asList(new Itinerary("S1", valueOf(1.4)), new Itinerary("S2", valueOf(3.3)), new Itinerary("S3", valueOf(1.0))));
        providerItineraries.put(providers.get(2), Arrays.asList(new Itinerary("W1", valueOf(1.6)), new Itinerary("W2", valueOf(5.2)), new Itinerary("W3", valueOf(3.1))));

        itineraryService = new ItineraryService(new MockProviderService(providerItineraries), new ItineraryAggretateSelector());

        providerItinerarySearchRequest = providers.stream()
                .map(provider -> ProviderItinerariesSearchRequest.create(itinerarySearchRequest, provider))
                .collect(Collectors.toList());
    }

    @Test
    public void test() throws InterruptedException {
        itineraryService.cheaperItineraryByProvider(providerItinerarySearchRequest)
                .subscribe(ItineraryServiceTest::printData);
    Thread.sleep(10000);
    }

    public static void printData(Itinerary itinerary) {
        System.out.println(itinerary);
        System.out.println("Thread: " + Thread.currentThread());
    }

    /* IO, mas threads que cores . #of cores/ (1 -blocking factor(0..1))*/
}